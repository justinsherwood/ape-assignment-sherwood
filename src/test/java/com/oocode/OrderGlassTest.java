package com.oocode;

import org.junit.Test;
import okhttp3.*;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class OrderGlassTest {
    @Test
    public void checkPlaceOrder() throws Exception {
        OrderGlass orderGlass = mock(OrderGlass.class);

        try {
            orderGlass.placeOrder(new String[]{"123", "456", "789", "Churchill"}, "test");
            orderGlass.placeOrder(new String[]{"48", "36", "1", "Victoria"}, "test");
        } catch(Exception e) {
            fail("Order failed to be successfully placed.");
        }
    }

    @Test
    public void checkCallLargeOrderEndpoint() throws Exception {
        OrderGlass orderGlass = mock(OrderGlass.class);

        // assert that a large order of toughened glass is placed
        assertEquals(orderGlass.callLargeOrderEndpoint(1, 121, 150), true);

        // assert that a small order of toughened glass is placed
        assertEquals(orderGlass.callLargeOrderEndpoint(1, 121, 140), false);

        // assert that a large order of plain glass is placed
        assertEquals(orderGlass.callLargeOrderEndpoint(1, 1, 20001), true);

        // assert that a small order of plain glass is placed
        assertEquals(orderGlass.callLargeOrderEndpoint(1, 1, 20000), false);
    }

    @Test
    public void checkApiIntegration() throws Exception {
        OrderGlass orderGlass = mock(OrderGlass.class);
        BodyBuilder bodyBuilder = mock(BodyBuilder.class);

        RequestBody toughenedGlassRequestBody = bodyBuilder.orderGlass("test", 1, 121, 150, "toughened");
        RequestBody plainGlassRequestBody = bodyBuilder.orderGlass("test",1, 1, 20000, "plain");

        // assert that a large order of toughened glass can successfully be made
        assertEquals(orderGlass.sendOrderRequest(toughenedGlassRequestBody, true), true);

        // assert that a small order of plain glass can successfully be made
        assertEquals(orderGlass.sendOrderRequest(plainGlassRequestBody, false), true);
    }

    @Test
    public void checkGetFrameThicknessByWindowModel() throws Exception {
        OrderGlass orderGlass = mock(OrderGlass.class);

        // assert that the correct windowFrameThicknessWidth by windowModel is returned
        assertEquals(orderGlass.getFrameThicknessByWindowModel("Churchill", true), 4);
        assertEquals(orderGlass.getFrameThicknessByWindowModel("Victoria", true), 2);
        assertEquals(orderGlass.getFrameThicknessByWindowModel("Albert", true), 3);

        // assert that the correct windowFrameThicknessHeight by windowModel is returned
        assertEquals(orderGlass.getFrameThicknessByWindowModel("Churchill", false), 3);
        assertEquals(orderGlass.getFrameThicknessByWindowModel("Victoria", false), 3);
        assertEquals(orderGlass.getFrameThicknessByWindowModel("Albert", false), 4);

        // assert that a null Exception value is set when sending unknown windowModel
        try {
            orderGlass.getFrameThicknessByWindowModel("Unknown", true);
            fail("Unknown model of glass exception not thrown.");
        } catch(Exception e) {
            assertNotNull(e.getMessage());
        }
        try {
            orderGlass.getFrameThicknessByWindowModel("Unknown", false);
            fail("Unknown model of glass exception not thrown.");
        } catch(Exception e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void checkGetFrameThicknessHeightByWindowModel() throws Exception {
        OrderGlass orderGlass = mock(OrderGlass.class);

        // assert that the correct windowFrameThicknessHeight by windowModel is returned
        assertEquals(orderGlass.getFrameThicknessHeightByWindowModel("Churchill"), 3);
        assertEquals(orderGlass.getFrameThicknessHeightByWindowModel("Victoria"), 3);
        assertEquals(orderGlass.getFrameThicknessHeightByWindowModel("Albert"), 4);

        // assert that a null Exception value is set when sending unknown windowModel
        try {
            orderGlass.getFrameThicknessByWindowModel("Unknown", false);
            fail("Unknown model of glass exception not thrown.");
        } catch(Exception e) {
            assertNotNull(e.getMessage());
        }
    }

    @Test
    public void checkUseToughenedGlass() throws Exception {
        OrderGlass orderGlass = mock(OrderGlass.class);

        // assert that toughened glass is used when > 120 inches and > 3000 square inches
        assertEquals(orderGlass.useToughenedGlass(25, 121), true);

        // assert that toughened glass is used when <= 120 inches and > 3000 square inches
        assertEquals(orderGlass.useToughenedGlass(26, 120), true);

        // assert that toughened glass is used when > 120 inches and <= 3000 square inches
        assertEquals(orderGlass.useToughenedGlass(1, 121), true);

        // assert that plain glass is used when < 120 inches and < 3000 square inches
        assertEquals(orderGlass.useToughenedGlass(1, 120), false);
    }
}