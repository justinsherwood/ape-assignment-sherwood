package com.oocode;

import okhttp3.*;

public class OrderGlass {
    public static void placeOrder(String[] args, String account) throws Exception {
        int windowWidth = Integer.parseInt(args[0]);  // the width of the window
        int windowHeight = Integer.parseInt(args[1]);  // the height of the window
        int windowOrderCount = Integer.parseInt(args[2]);  // the number of windows of this size
        String windowModel = args[3];                 // the model name of these windows

        // the thickness of the frame depends on the model of window
        int windowFrameThicknessWidth = getFrameThicknessByWindowModel(windowModel, true);
        int windowFrameThicknessHeight = getFrameThicknessByWindowModel(windowModel, false);

        // the glass pane is the size of the window minus allowance for
        // the thickness of the frame
        int netWindowWidth = (windowWidth - windowFrameThicknessWidth);
        int netWindowHeight = (windowHeight - windowFrameThicknessHeight);

        RequestBody requestBody = BodyBuilder.orderGlass(account, netWindowWidth, netWindowHeight, windowOrderCount, "plain");
        if (useToughenedGlass(netWindowWidth, netWindowHeight))
            requestBody = BodyBuilder.orderGlass(account, netWindowWidth, netWindowHeight, windowOrderCount, "toughened");

        boolean shouldCallLargeOrderEndpoint = callLargeOrderEndpoint(netWindowWidth, netWindowHeight, windowOrderCount);

        sendOrderRequest(requestBody, shouldCallLargeOrderEndpoint);
    }

    public static int getFrameThicknessByWindowModel(String windowModel, boolean getThicknessWidth) throws Exception {
        if (!getThicknessWidth) return getFrameThicknessHeightByWindowModel(windowModel);
        if (windowModel.equals("Churchill")) {
            return 4;
        }
        if (windowModel.equals("Victoria")) {
            return 2;
        }
        if (windowModel.equals("Albert")) {
            return 3;
        }
        throw new Exception("Model name is not known.");
    }

    public static int getFrameThicknessHeightByWindowModel(String windowModel) throws Exception {
        if (windowModel.equals("Churchill")) return 3;
        if (windowModel.equals("Victoria")) {
            return 3;
        }
        if (windowModel.equals("Albert")) {
            return 4;
        }
        throw new Exception("Model name is not known.");
    }

    public static boolean useToughenedGlass(int netWindowWidth, int netWindowHeight) {
        // use toughened glass if over 120 inches high or with an area of glass over 3000 square inches.
        if (netWindowHeight > 120 || netWindowWidth * netWindowHeight > 3000) return true;
        return false;
    }

    public static boolean callLargeOrderEndpoint(int netWindowWidth, int netWindowHeight, int windowOrderCount) {
        // requirements for calling the large order endpoint for toughened glass
        if (netWindowHeight > 120 && netWindowWidth * netWindowHeight * windowOrderCount > 18000) return true;
        // requirements for calling the large order endpoint for plain glass
        if (netWindowHeight <= 120 && netWindowWidth * netWindowHeight * windowOrderCount > 20000) return true;
        return false;
    }

    public static boolean sendOrderRequest(RequestBody requestBody, boolean shouldCallLargeOrderEndpoint) throws Exception {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://immense-fortress-19979.herokuapp.com/order")
                .method("POST", RequestBody.create(null, new byte[0]))
                .post(requestBody)
                .build();

        if (shouldCallLargeOrderEndpoint) {
            request = new Request.Builder()
                    .url("https://immense-fortress-19979.herokuapp.com/large-order")
                    .method("POST", RequestBody.create(null, new byte[0]))
                    .post(requestBody)
                    .build();
        }

        try (Response response = client.newCall(request).execute()) {
            try (ResponseBody body = response.body()) {
                if (body != null) {
                    String bodyString = body.string();

                    if(bodyString.isEmpty()){
                        return false;
                    }

                    System.out.println(bodyString);
                    return true;
                }
                return false;
            }
        }
    }
}