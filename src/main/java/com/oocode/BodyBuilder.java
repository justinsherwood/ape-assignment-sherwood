package com.oocode;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class BodyBuilder {
    public static RequestBody orderGlass(String account, int netWindowWidth, int netWindowHeight, int windowOrderCount, String type) {
        return new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("account", account)
                .addFormDataPart("quantity", Integer.toString(windowOrderCount))
                .addFormDataPart("width", Integer.toString(netWindowWidth))
                .addFormDataPart("height", Integer.toString(netWindowHeight))
                .addFormDataPart("type", type)
                .build();
    }
}
